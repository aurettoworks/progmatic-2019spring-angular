import { by, element, ElementFinder } from 'protractor';

export class StudentModifyPage {

    getModifyModal(): ElementFinder {
        return element(by.css('app-modify-modal'));
    }

    getModifyModalNameInput(): ElementFinder {
        return element(by.css('app-modify-modal #exampleInputName'));
    }

    getModifyModalNameInputValue(): Promise<string> {
        return this.getModifyModalNameInput().getAttribute('value') as Promise<string>;
    }

    getModifyModalSubmitButton(): ElementFinder {
        return element(by.css('app-modify-modal .modal-footer .btn'));
    }

    getModifyModalCloseButton(): ElementFinder {
        return element(by.css('app-modify-modal .modal-header .close'));
    }

}
