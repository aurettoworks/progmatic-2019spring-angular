import { AppPage } from './app.po';
import { StudentModifyPage } from './student-modify.po';
import { browser, logging, ElementFinder } from 'protractor';

describe('Student modify modal', () => {
    let page: AppPage;
    let smPage: StudentModifyPage;
    let row: ElementFinder;
    let oldName: Promise<string>;
    let newName: string;

    function openAndFillModal() {
        page.navigateTo();
        row = page.getLastRow();
        newName = 'E2E Teszt Tódor';
        oldName = page.getNthTdTextOfRow(row, 1);
        page.getButtonOfRowWithClass(row, 'update-button').click();
        smPage.getModifyModalNameInput().clear();
        smPage.getModifyModalNameInput().sendKeys(newName);
    }

    beforeEach(() => {
        page = new AppPage();
        smPage = new StudentModifyPage();
    });

    it('should copy data from table row', () => {
        page.navigateTo();
        row = page.getLastRow();
        const name = page.getNthTdTextOfRow(row, 1);
        // const email = page.getNthTdTextOfRow(row, 2);
        // const age = page.getNthTdTextOfRow(row, 3);
        // const gender = page.getNthTdTextOfRow(row, 4);
        page.getButtonOfRowWithClass(row, 'update-button').click();
        expect(smPage.getModifyModal().isPresent()).toBeTruthy();
        expect(smPage.getModifyModalNameInputValue()).toBe(name);
    });

    it('should update a student', () => {
        openAndFillModal();
        smPage.getModifyModalSubmitButton().click();
        expect(page.getNthTdTextOfRow(row, 1)).toBe(newName);
    });

    it('should not update a student if modal is closed', () => {
        openAndFillModal();
        smPage.getModifyModalCloseButton().click();
        expect(page.getNthTdTextOfRow(row, 1)).toBe(oldName);
    });

    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
