import { browser, by, element, ElementFinder } from 'protractor';

export class AppPage {
    navigateTo() {
        return browser.get(browser.baseUrl) as Promise<any>;
    }

    getTitleText(): Promise<string> {
        return element(by.css('app-root h1')).getText() as Promise<string>;
    }

    getNumberOfStudentRows(): Promise<number> {
        return element.all(by.css('table tbody tr')).count() as Promise<number>;
    }

    getAddButton(): ElementFinder {
        return element(by.css('app-header .btn.btn-success'));
    }
    getAddNameInput(): ElementFinder {
        return element(by.id('exampleInputName'));
    }
    getAddEmailInput(): ElementFinder {
        return element(by.id('exampleInputEmail1'));
    }
    getAddAgeInput(): ElementFinder {
        return element(by.id('exampleInputAge'));
    }
    getAddGenderInput(): ElementFinder {
        return element(by.id('exampleInputGender'));
    }
    getAddSubmitButton(): ElementFinder {
        return element(by.css('app-add-student .btn.btn-success'));
    }

    getLastRow(): ElementFinder {
        return element(by.css('table tbody tr:last-child'));
    }
    getButtonOfRowWithClass(row: ElementFinder, className: string): ElementFinder {
        return row.$('.' + className);
    }
    getNthTdTextOfRow(row: ElementFinder, n: number): Promise<string> {
        return row.$(`td:nth-child(${n})`).getText() as Promise<string>;
    }
    getModalDeleteButton(): ElementFinder {
        return element(by.css('.modal-footer .btn'));
    }
}
