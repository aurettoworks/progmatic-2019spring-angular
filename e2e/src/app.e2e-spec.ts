import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
    let page: AppPage;

    beforeEach(() => {
        page = new AppPage();
    });

    it('should display welcome message', () => {
        page.navigateTo();
        expect(page.getTitleText()).toMatch(/Hallgatói nyilvántartás \(\d+ db\)/);
    });

    it('should navigate to add form', () => {
        page.navigateTo();
        expect(page.getAddButton().isPresent()).toBeTruthy();
        page.getAddButton().click();
        expect(page.getTitleText()).toEqual('Hallgató hozzáadása');
        expect(page.getAddNameInput().isPresent()).toBeTruthy();
    });

    it('should add a new student', () => {
        page.navigateTo();
        const increasedNumberOfRows = page.getNumberOfStudentRows().then( n => n + 1 );
        page.getAddButton().click();
        page.getAddNameInput().sendKeys('Kis Jóska');
        page.getAddEmailInput().sendKeys('kis-joska@progmatic.hu');
        page.getAddAgeInput().sendKeys(25);
        page.getAddGenderInput().sendKeys('férfi');
        page.getAddSubmitButton().click();
        expect(page.getTitleText()).toMatch(/Hallgatói nyilvántartás \(\d+ db\)/);
        expect(page.getNumberOfStudentRows()).toBe(increasedNumberOfRows);
    });

    it('should delete a student', () => {
        page.navigateTo();
        const numberOfRows = page.getNumberOfStudentRows();
        page.getButtonOfRowWithClass( page.getLastRow(), 'delete-button' ).click();
        page.getModalDeleteButton().click();
        expect(page.getNumberOfStudentRows()).toBe(numberOfRows.then( n => n - 1 ));
    });

    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
