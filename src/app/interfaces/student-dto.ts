import { Student } from './student';

export interface StudentDTO {
    success: boolean;
    students: Student[];
}
