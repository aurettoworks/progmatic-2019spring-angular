import { Book } from './book';

export enum Gender {
    MALE = 'MALE',
    FEMALE = 'FEMALE',
}

export interface Student {
    id?: number;
    name: string;
    email: string;
    age: number;
    gender: Gender;
    books: Book[];
}

export interface KeyValue {
    key: string;
    value: string;
}
