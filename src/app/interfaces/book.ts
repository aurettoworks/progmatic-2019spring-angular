export interface Book {
    id: number;
    isbn: string;
    title: string;
    subtitle: string;
    author: string;
    website: string;
}
