import { Injectable } from '@angular/core';

import { Gender, KeyValue } from './interfaces/student';

@Injectable({
    providedIn: 'root'
})
export class GenderService {

    constructor() { }

    getGenderArray(): KeyValue[] {
        // [ { key: 'FEMALE', value: 'nő' }, ... ]
        const genderValues: KeyValue[] = [];
        for (const genderKey of Object.keys(Gender)) {
            genderValues.push({ key: genderKey, value: Gender[genderKey] });
        }
        return genderValues;
    }
}
