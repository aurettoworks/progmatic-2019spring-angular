import { Pipe, PipeTransform } from '@angular/core';

import { Student } from '../interfaces/student';

@Pipe({
    name: 'studentSearch'
})
export class StudentSearchPipe implements PipeTransform {

    transform(students: Student[], searchTerm: string): Student[] {
        return searchTerm === '' ? students :
            students.filter(s =>  s.name.toLocaleLowerCase().includes(searchTerm.toLocaleLowerCase()));
    }

}
