import { Injectable } from '@angular/core';

import { Student } from './interfaces/student';

@Injectable({
    providedIn: 'root'
})
export class StudentStorageService {

    public student: Student;

    constructor() { }
}
