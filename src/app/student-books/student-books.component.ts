import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Student } from '../interfaces/student';
import { StudentHTTPService } from '../student-http.service';
import { StudentStorageService } from '../student-storage.service';

@Component({
    selector: 'app-student-books',
    templateUrl: './student-books.component.html',
    styleUrls: ['./student-books.component.scss']
})
export class StudentBooksComponent implements OnInit {

    id: number;
    student: Student;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private studentService: StudentHTTPService,
        private studentStorageService: StudentStorageService
    ) {
        this.id = + this.route.snapshot.paramMap.get('student_id');
        if (isNaN(this.id)) {
            this.router.navigate(['students']);
        }
    }

    ngOnInit() {
        this.student = this.studentStorageService.student;
        if (this.student === undefined) {
            this.student = {
                name: '',
                email: '',
                age: null,
                gender: null,
                books: []
            };
            this.studentService.getStudent(this.id)
                .then( s => { this.student = s; } )
                .catch( () => { this.router.navigate(['students']); } );
        }
    }

}
