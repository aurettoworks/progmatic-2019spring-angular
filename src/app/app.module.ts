import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentListComponent } from './student-list/student-list.component';
import { HeaderComponent } from './header/header.component';
import { StudentRowComponent } from './student-list/student-row/student-row.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { DeleteModalComponent } from './student-list/delete-modal/delete-modal.component';
import { ModifyModalComponent } from './student-list/modify-modal/modify-modal.component';
import { GenderPipe } from './pipes/gender.pipe';
import { StudentSearchPipe } from './pipes/student-search.pipe';
import { StudentBooksComponent } from './student-books/student-books.component';

@NgModule({
    entryComponents: [
        DeleteModalComponent,
        ModifyModalComponent
    ],
    declarations: [
        AppComponent,
        StudentListComponent,
        HeaderComponent,
        StudentRowComponent,
        AddStudentComponent,
        DeleteModalComponent,
        ModifyModalComponent,
        GenderPipe,
        StudentSearchPipe,
        StudentBooksComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        AppRoutingModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
