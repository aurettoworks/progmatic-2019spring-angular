import { TestBed } from '@angular/core/testing';

import { StudentStorageService } from './student-storage.service';

describe('StudentStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentStorageService = TestBed.get(StudentStorageService);
    expect(service).toBeTruthy();
  });
});
