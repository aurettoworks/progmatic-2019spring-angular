import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { GenderService } from '../gender.service';
import { StudentHTTPService } from '../student-http.service';
import { Gender, Student, KeyValue } from '../interfaces/student';

@Component({
    selector: 'app-add-student',
    templateUrl: './add-student.component.html',
    styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit {

    student: Student;
    genders: KeyValue[];
    errors: string[];

    constructor(private genderService: GenderService, private studentService: StudentHTTPService, private router: Router) {
        this.student = {
            name: '',
            email: '',
            age: null,
            gender: Gender.FEMALE,
            books: []
        };
        this.genders = this.genderService.getGenderArray();
        this.errors = [];
    }

    ngOnInit() { }

    submit(): void {
        this.studentService.addStudent(this.student)
            .then(() => {
                this.router.navigate(['students']);
            })
            .catch(studentError => {
                this.errors = studentError.errorInfos;
            });
    }

}
