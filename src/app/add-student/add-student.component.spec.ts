import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AddStudentComponent } from './add-student.component';
import { HeaderComponent } from '../header/header.component';
import { GenderPipe } from '../pipes/gender.pipe';

describe('AddStudentComponent', () => {
    let component: AddStudentComponent;
    let fixture: ComponentFixture<AddStudentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpClientTestingModule, RouterTestingModule],
            declarations: [AddStudentComponent, HeaderComponent, GenderPipe]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddStudentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
