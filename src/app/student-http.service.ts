import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Student } from './interfaces/student';
import { StudentDTO } from './interfaces/student-dto';
import { StudentError } from './errors/student-error';

@Injectable({
    providedIn: 'root'
})
export class StudentHTTPService {

    private readonly URL = 'https://progmatic.hu/frontend/students-with-books';

    constructor(private http: HttpClient) { }

    private transformStudentDTO(serverData: StudentDTO): Student[] {
        if (!serverData.success) {
            throw new StudentError(serverData['error-infos']);
        }
        return serverData.students;
    }

    getStudent(studentId: number): Promise<Student> {
        return this.http.get(this.URL + '?id=' + studentId, { withCredentials: true })
            .toPromise()
            .then(this.transformStudentDTO)
            .then(values => values[0]);
    }

    getStudents(): Promise<Student[]> {
        return this.http.get(this.URL, { withCredentials: true }).toPromise()
            // .then( (serverData: StudentDTO) => serverData.students );
            .then(this.transformStudentDTO);
    }

    deleteStudent(studentId: number): Promise<Student[]> {
        return this.http.delete(this.URL + '?id=' + studentId, { withCredentials: true })
            .toPromise().then(this.transformStudentDTO);
    }

    addStudent(student: Student): Promise<Student[]> {
        // clone method 1
        // student = Object.assign({}, student);
        // clone method 2
        // student = JSON.parse( JSON.stringify(student) );
        return this.http.post(this.URL, { student }, { withCredentials: true }).toPromise()
            .then(this.transformStudentDTO);
    }

    modifyStudent(student: Student): Promise<Student[]> {
        return this.http.put(this.URL + '?id=' + student.id, { student }, { withCredentials: true })
            .toPromise().then(this.transformStudentDTO);
    }
}
