import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { StudentHTTPService } from './student-http.service';

describe('StudentHTTPService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            HttpClientTestingModule
        ]
    }).compileComponents());

    it('should be created', () => {
        const service: StudentHTTPService = TestBed.get(StudentHTTPService);
        expect(service).toBeTruthy();
    });
});
