import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ModifyModalComponent } from './modify-modal.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GenderService } from '../../gender.service';
import { GenderPipe } from '../../pipes/gender.pipe';

describe('ModifyModalComponent', () => {
    let component: ModifyModalComponent;
    let fixture: ComponentFixture<ModifyModalComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ModifyModalComponent, GenderPipe],
            imports: [FormsModule, HttpClientTestingModule],
            providers: [NgbActiveModal, GenderService]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ModifyModalComponent);
        component = fixture.componentInstance;
        component.student = {
            name: '',
            email: '',
            age: null,
            gender: null,
            books: []
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should validate names', () => {
        component.student.name = '';
        expect(component.isNameValid()).toEqual(false);
        component.student.name = 'Kis Jóska';
        expect(component.isNameValid()).toEqual(true);
        component.student.name = 'a';
        expect(component.isNameValid()).toEqual(true);
        component.student.name = ' ';
        expect(component.isNameValid()).toEqual(false);
        component.student.name = '       ';
        expect(component.isNameValid()).toEqual(false);
    });

    it('should validate ages', () => {
        component.student.age = -1;
        expect(component.isAgeValid()).toEqual(false);
        component.student.age = 130;
        expect(component.isAgeValid()).toEqual(false);
        component.student.age = 0;
        expect(component.isAgeValid()).toEqual(false);
        component.student.age = 33.5;
        expect(component.isAgeValid()).toEqual(false);
        component.student.age = '101kiskutya' as unknown as number;
        expect(component.isAgeValid()).toEqual(false);
        component.student.age = 'kiskutya' as unknown as number;
        expect(component.isAgeValid()).toEqual(false);
        component.student.age = '101' as unknown as number;
        expect(component.isAgeValid()).toEqual(true);
        component.student.age = NaN;
        expect(component.isAgeValid()).toEqual(false);
        component.student.age = Infinity;
        expect(component.isAgeValid()).toEqual(false);
        component.student.age = '0b101' as unknown as number;
        expect(component.isAgeValid()).toEqual(false);
    });
    it('should validate emails', () => {
        component.student.email = '';
        expect(component.isEmailValid()).toEqual(false);
        component.student.email = 'asd/-asd@asd.hu';
        expect(component.isEmailValid()).toEqual(false);
        component.student.email = 'asd-asd@asd.hu';
        expect(component.isEmailValid()).toEqual(true);
        component.student.email = 'asd.asd@asd.hu';
        expect(component.isEmailValid()).toEqual(true);
        component.student.email = 'a@asd.hu';
        expect(component.isEmailValid()).toEqual(true);
    });
});
