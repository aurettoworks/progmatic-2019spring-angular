import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { KeyValue, Student } from '../../interfaces/student';
import { GenderService } from '../../gender.service';
import { StudentHTTPService } from '../../student-http.service';

@Component({
    selector: 'app-modify-modal',
    templateUrl: './modify-modal.component.html',
    styleUrls: ['./modify-modal.component.scss']
})
export class ModifyModalComponent implements OnInit {

    @Input()
    student: Student;
    errors: string[];
    genders: KeyValue[];
    showNetworkAlert: boolean;

    constructor(private genderService: GenderService, public activeModal: NgbActiveModal, private studentService: StudentHTTPService) {
        this.errors = [];
        this.genders = this.genderService.getGenderArray();
        this.showNetworkAlert = false;
    }

    ngOnInit() { }

    isNameValid(): boolean {
        return this.student.name.trim() !== '';
    }

    isEmailValid(): boolean {
        const emailRegexp = /^(([^/<>()\[\]\\.,;:\s@"]+(\.[^/<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // tslint:disable-line
        return this.student.email !== '' && emailRegexp.test(this.student.email);
    }

    isAgeValid(): boolean {
        return !isNaN(parseInt('' + this.student.age, 10))
            && /^[1-9]\d*$/.test('' + this.student.age)
            && this.student.age <= 120;
    }

    save(): void {
        this.showNetworkAlert = false;
        this.studentService.modifyStudent(this.student).then(students => {
            this.activeModal.close(students);
        }).catch(() => {
            this.showNetworkAlert = true;
        });
    }

}
