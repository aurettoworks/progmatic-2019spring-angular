import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';

import { StudentListComponent } from './student-list.component';
import { HeaderComponent } from '../header/header.component';
import { StudentSearchPipe } from '../pipes/student-search.pipe';
import { GenderPipe } from '../pipes/gender.pipe';
import { StudentRowComponent } from './student-row/student-row.component';

describe('StudentListComponent', () => {
    let component: StudentListComponent;
    let fixture: ComponentFixture<StudentListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule, FormsModule],
            declarations: [
                StudentListComponent,
                HeaderComponent,
                GenderPipe,
                StudentSearchPipe,
                StudentRowComponent
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
