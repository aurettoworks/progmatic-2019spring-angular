import { Component, OnInit } from '@angular/core';

import { Student } from '../interfaces/student';
import { StudentHTTPService } from '../student-http.service';

@Component({
    selector: 'app-student-list',
    templateUrl: './student-list.component.html',
    styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {

    students: Student[];
    isLoading: boolean;
    searchTerm: string;

    constructor(private studentService: StudentHTTPService) {
        this.students = [];
        this.isLoading = true;
        this.searchTerm = '';
    }

    ngOnInit() {
        this.studentService.getStudents().then(students => {
            this.students = students;
            this.isLoading = false;
        });
    }

    refreshStudents(students: Student[]) {
        this.students = students;
    }

}
