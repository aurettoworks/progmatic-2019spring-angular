import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Student } from '../../interfaces/student';
import { StudentHTTPService } from '../../student-http.service';
import { StudentStorageService } from '../../student-storage.service';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { ModifyModalComponent } from '../modify-modal/modify-modal.component';

@Component({
    selector: '[app-student-row]', // tslint:disable-line
    templateUrl: './student-row.component.html',
    styleUrls: ['./student-row.component.scss']
})
export class StudentRowComponent implements OnInit {

    @Input()
    student: Student;
    @Output()
    refresh: EventEmitter<Student[]> = new EventEmitter();

    constructor(
        private studentService: StudentHTTPService,
        private modalService: NgbModal,
        private studentStorageService: StudentStorageService
    ) { }

    ngOnInit() {
    }

    delete(): void {
        const modalRef = this.modalService.open(DeleteModalComponent);
        modalRef.result.then(() => {
            this.studentService.deleteStudent(this.student.id).then(students => {
                this.refresh.emit(students);
            });
        }).catch(() => { });
    }

    modify(): void {
        const modalRef = this.modalService.open(ModifyModalComponent);
        modalRef.componentInstance.student = Object.assign({}, this.student);
        modalRef.result.then(students => {
            this.refresh.emit(students);
        }).catch(() => { });
    }

    store(): void {
        this.studentStorageService.student = this.student;
    }

}
