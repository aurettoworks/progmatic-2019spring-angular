import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { StudentRowComponent } from './student-row.component';
import { GenderPipe } from '../../pipes/gender.pipe';

describe('StudentRowComponent', () => {
    let component: StudentRowComponent;
    let fixture: ComponentFixture<StudentRowComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule],
            declarations: [StudentRowComponent, GenderPipe]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentRowComponent);
        component = fixture.componentInstance;
        component.student = {
            name: '',
            email: '',
            age: null,
            gender: null,
            books: []
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
