import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentListComponent } from './student-list/student-list.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { StudentBooksComponent } from './student-books/student-books.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'students' },
    { path: 'students', component: StudentListComponent },
    { path: 'add-student', component: AddStudentComponent },
    { path: 'student/:student_id/books', component: StudentBooksComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
