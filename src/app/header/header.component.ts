import { Component, Input, OnInit } from '@angular/core';

import { HeaderButtons } from '../interfaces/header-buttons';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    @Input()
    title: string;

    @Input()
    buttons: HeaderButtons;

    constructor() { }

    ngOnInit() {
    }

}
